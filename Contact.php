<?php

class Contact {
    
    private $name;
    private $phone;
    
    function setName($name) {
        $this->name=$name;
        return $this->name;
    }
    function setPhone($phone) {
        $this->phone=$phone;
        return $this->phone;
    }
    function getName() {
        return $this->name;
    }
    function getPhone() {
        return $this->phone;
    }
    
}

?>
